use anyhow::anyhow;
use image::io::Reader as ImageReader;
use image::{DynamicImage, GenericImage, GenericImageView, Pixel, Rgba};
use std::path::Path;

const GRAYSCALE_RED_FACTOR: f64 = 0.299;
const GRAYSCALE_GREEN_FACTOR: f64 = 0.587;
const GRAYSCALE_BLUE_FACTOR: f64 = 0.114;

fn main() {
    let first_img_path = "../../datasets/exemplos/circulos_1.png";
    let second_img_path = "../../datasets/exemplos/circulos_2.png";
    let img1 = decode_img(first_img_path.as_ref()).unwrap();
    let img2 = decode_img(second_img_path.as_ref()).unwrap();
    let img_gray = weighted_grayscale(&img1);
    let img_threshold = threshold(&img1, 190);
    let img_add = add(&img2, &img1).unwrap();
    let img_sub = subtract(&img2, &img1).unwrap();
    let img_mul = multiply(&img_threshold, &img2).unwrap();
    let img_div = divide(&img_threshold, &img1).unwrap();
    img_gray
        .save(Path::new("./output/gray.png"))
        .expect("Failed saving image!");
    img_threshold
        .save(Path::new("./output/threshold.png"))
        .expect("Failed saving image!");
    img_add
        .save(Path::new("./output/add.png"))
        .expect("Failed saving image!");
    img_sub
        .save(Path::new("./output/sub.png"))
        .expect("Failed saving image!");
    img_mul
        .save(Path::new("./output/mul.png"))
        .expect("Failed saving image!");
    img_div
        .save(Path::new("./output/div.png"))
        .expect("Failed saving image!");
}

fn decode_img(path: &std::path::Path) -> anyhow::Result<DynamicImage> {
    Ok(ImageReader::open(&path)?.decode()?)
}

fn weighted_grayscale(img: &DynamicImage) -> DynamicImage {
    let mut output_img = DynamicImage::new_rgba8(img.width(), img.height());
    for px in img.pixels() {
        let mut rgb_value: u8;
        rgb_value = (px.2[0] as f64 * GRAYSCALE_RED_FACTOR) as u8;
        rgb_value += (px.2[1] as f64 * GRAYSCALE_GREEN_FACTOR) as u8;
        rgb_value += (px.2[2] as f64 * GRAYSCALE_BLUE_FACTOR) as u8;
        // Copu alpha channel
        output_img.put_pixel(
            px.0,
            px.1,
            Rgba::from([rgb_value, rgb_value, rgb_value, px.2[2]]),
        );
    }
    output_img
}

fn threshold(img: &DynamicImage, luminance: u8) -> DynamicImage {
    let mut output_img = DynamicImage::new_rgba8(img.width(), img.height());
    for px in img.pixels() {
        let mut px_mut = px;
        let px_luminance = px.2.to_luma().0[0];
        px_mut.2 = match px_luminance > luminance {
            true => Rgba::from([u8::MAX; 4]),
            false => Rgba::from([u8::MIN; 4]),
        };
        output_img.put_pixel(px_mut.0, px_mut.1, px_mut.2)
    }
    output_img
}

fn subtract(img_lhs: &DynamicImage, img_rhs: &DynamicImage) -> anyhow::Result<DynamicImage> {
    check_matching_dimensions(img_lhs.dimensions(), img_rhs.dimensions())?;
    let mut output_img = DynamicImage::new_rgba8(img_lhs.width(), img_lhs.height());
    for px in img_lhs.pixels() {
        let rgba_lhs = px.2.to_rgba().0;
        let rgba_rhs = img_rhs.get_pixel(px.0, px.1).to_rgba().0;
        let mut rgba = [255; 4];
        for channel in 0..3 {
            rgba[channel] = rgba_lhs[channel].saturating_sub(rgba_rhs[channel]);
        }
        // Copy alpha channel from lhs image
        rgba[3] = px.2[3];
        output_img.put_pixel(px.0, px.1, Rgba::from(rgba));
    }
    Ok(output_img)
}

fn add(img_lhs: &DynamicImage, img_rhs: &DynamicImage) -> anyhow::Result<DynamicImage> {
    check_matching_dimensions(img_lhs.dimensions(), img_rhs.dimensions())?;
    let mut output_img = DynamicImage::new_rgba8(img_lhs.width(), img_lhs.height());
    for w in 0..img_lhs.width() {
        for h in 0..img_lhs.height() {
            let rgba_lhs = img_lhs.get_pixel(w, h).to_rgba().0;
            let rgba_rhs = img_rhs.get_pixel(w, h).to_rgba().0;
            let mut rgba = [255; 4];
            for channel in 0..=2 {
                rgba[channel] = rgba_lhs[channel].saturating_add(rgba_rhs[channel]);
            }
            output_img.put_pixel(w, h, Rgba::from(rgba));
        }
    }
    Ok(output_img)
}

fn multiply(img_lhs: &DynamicImage, img_rhs: &DynamicImage) -> anyhow::Result<DynamicImage> {
    check_matching_dimensions(img_lhs.dimensions(), img_rhs.dimensions())?;
    let mut output_img = DynamicImage::new_rgba8(img_lhs.width(), img_lhs.height());
    for w in 0..img_lhs.width() {
        for h in 0..img_lhs.height() {
            let rgba_lhs = img_lhs.get_pixel(w, h).to_rgba().0;
            let rgba_rhs = img_rhs.get_pixel(w, h).to_rgba().0;
            let mut rgba = [255; 4];
            for channel in 0..=2 {
                rgba[channel] = rgba_lhs[channel].saturating_mul(rgba_rhs[channel]);
            }
            output_img.put_pixel(w, h, Rgba::from(rgba));
        }
    }
    Ok(output_img)
}

fn divide(img_lhs: &DynamicImage, img_rhs: &DynamicImage) -> anyhow::Result<DynamicImage> {
    check_matching_dimensions(img_lhs.dimensions(), img_rhs.dimensions())?;
    let mut output_img = DynamicImage::new_rgba8(img_lhs.width(), img_lhs.height());
    for w in 0..img_lhs.width() {
        for h in 0..img_lhs.height() {
            let rgba_lhs = img_lhs.get_pixel(w, h).to_rgba().0;
            let rgba_rhs = img_rhs.get_pixel(w, h).to_rgba().0;
            let mut rgba = [255; 4];
            for channel in 0..=2 {
                rgba[channel] = rgba_lhs[channel]
                    .checked_div(rgba_rhs[channel])
                    .unwrap_or(0);
            }
            output_img.put_pixel(w, h, Rgba::from(rgba));
        }
    }
    Ok(output_img)
}

fn check_matching_dimensions(lhs: (u32, u32), rhs: (u32, u32)) -> anyhow::Result<()> {
    if lhs != rhs {
        return Err(anyhow!(
            "lhs image dimensions ({:?}) don't match rhs image dimensions ({:?})",
            lhs,
            rhs
        ));
    }
    Ok(())
}
